FROM python:3.13-alpine
RUN pip3 install packt 
VOLUME [ "/app", "/data" ]
WORKDIR /app
ENV PACKT_CLI_ARGS --help
ENTRYPOINT sh -c "/usr/local/bin/packt-cli ${PACKT_CLI_ARGS}" 
